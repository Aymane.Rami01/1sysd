#include <stdio.h>
#include <math.h>

double P(double r) {
    return 2 * M_PI * r;
}

double S(double r) {
    return M_PI * (r * r);
}

int main() {
    double r;

    printf("Entrer le rayon du cercle : ");
    scanf("%lf", &r);

    printf("Le périmètre du cercle est : %.2f\n", P(r));
    printf("La surface du disque est : %.2f\n", S(r));

    return 0;
}