#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int factorial(int n) {
	if(n == 0 || n == 1){
		return 1;
	}else {
		return n * factorial(n - 1);
	}

}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    printf("Usage: ./factorial val1 val2 \n");
    return 1;
  }
  int a = strtold(argv[1], NULL);
  int b = strtold(argv[2], NULL);
  for(int i = a; i <= b; i++) {
  printf("%d!=  %d\n ", i, factorial(i));
  }
  return 0;
}
