#include<stdio.h>
#include<stdlib.h>

void display_histogram(int tab[], int size) {
    int i, j, max = tab[0];


    for (i = 1; i < size; i++) {
        if (tab[i] > max) {
            max = tab[i];
        }
    }


    for (i = max; i > 0; i--) {
        for (j = 0; j < size; j++) {
            if (tab[j] >= i) {
                printf("*");
            } else {
                printf(" ");
            }
        }
        printf("\n");
    }


    for (i = 0; i < size; i++) {
        printf("*");
    }
    printf("\n");
}

int main() {
    int values[] = { 4, 9, 8, 2, 0, 1, 10, 5, 8 };

    display_histogram(values, 9);

    exit(EXIT_SUCCESS);
}
