#include <stdio.h>
#include <ctype.h>

void invertcase(char *s) {
    for (int i = 0; s[i]!= '\0'; i++) {
        if (isalpha(s[i])) {
            if (isupper(s[i])) {
                s[i] = tolower(s[i]);
            } else {
                s[i] = toupper(s[i]);
            }
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc!= 2) {
        printf("Usage: %s <string>\n", argv[0]);
        return 1;
    }

    invertcase(argv[1]);
    printf("%s\n", argv[1]);

    return 0;
}